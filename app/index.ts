import server from './src/server'

process.on('unhandledRejection', (error: Error) => {
	//@ts-ignore
	logger.info("Unhandled exception - internal error. Stacktrace=" + error.stack);
});

// Main entry point.
server()
