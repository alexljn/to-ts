const express = require("express");
const formationsController = require('../controllers/formations')
/**
 * Returns all the routes related to the right resources controller
 * @returns {Router} The routes
 */
const router = express.Router({
	strict: true,
})

// TODO: Add other routes root here
router.use('/formations', formationsController)

module.exports = router
