/**
 * This middleware is used to catch all errors in Express Router and return a custom response for each case.
 */
const RequestNotValidError = require("../../errors/request-not-valid-error");
/**
 * Express Root Error Controller Advice.
 * A controller advice is where all errors are listed at a single point before being sent to the client.
 * @param fn Function to execute usually the last middleware in your express request route
 * @returns {(function(*=, *=, *=): void)|*} Express middleware function.
 */
module.exports = (fn) => (req, res, next) => {
	Promise.resolve(fn(req, res, next)).catch((err) => {
		const newVar = { name: err.constructor.name, message: err.message || err.nested && err.nested.message }
		console.error(newVar)
		// List all errors here and treat them
		if (err instanceof RequestNotValidError) {
			res.status(400).send(newVar)
		} else {
			// log the error
			res.status(500).send(newVar)
		}
	})
}
