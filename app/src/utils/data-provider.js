const csv = require('csv-parser')
const fs = require('fs')
const camelcaseKeys = require('camelcase-keys');

const datas = [];

console.info("Loading datas ...")
// Parse datas
fs.createReadStream(`${process.cwd()}/app/data/formations.csv`)
	.pipe(csv({separator: ';'}))
	.on('data', (data) => {
		datas.push(camelcaseKeys(data))
	})
	.on('end', () => {
		console.info(`Loaded items : ${datas.length}`)
	})
	.on('error', (err) => {
		console.error(err);
	});

module.exports = {
	datas
}
