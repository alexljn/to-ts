const repository = require('../db/formations')

/**
 * @typedef Formation
 * @property {string} id
 * @property {string} denomination
 * @property {string} siren
 * @property {string} siret
 * @property {string} voie
 * @property {string} code_postal
 * @property {string} ville
 * @property {string} label
 */
/**
 * Fetch all formations like label
 * @param {string} label
 * @returns {Array<Formation>} Gdp resources
 */
const findLikeLabel = (label) => repository.findLikeLabel(label)

/**
 * Fetch all formations like denomination
 * @param {string} name
 * @returns {Array<Formation>} Gdp resources
 */
const findLikeDenomination = (name) => repository.findLikeDenomination(name)

module.exports = {
	findLikeLabel,
	findLikeDenomination
}
