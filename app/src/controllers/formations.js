/**
 * Returns all the routes related to the GDP resource
 * @returns {Router} The routes
 */
const formationsService = require("../services/formations");
const express = require("express");
const withControllerAdvice = require('../router/middleware/controller-advice')
const RequestNotValidError = require("../errors/request-not-valid-error");

const router = express.Router()

/**
 * Search formation by query
 * For now, you can search by
 * @param {string} [label.query] Label of the formation (optional)
 * @param {string} [denomination.query] Denomination of the formation (optional)
 * @returns 200 : Success
 */
router.get("/",
	withControllerAdvice(async (req, res) => {
		const {label, denomination} = req.query;

		if (label) {
			res.status(200).json(formationsService.findLikeLabel(label))
		} else if (denomination) {
			res.status(200).json(formationsService.findLikeDenomination(denomination))
		} else {
			throw new RequestNotValidError("Missing query param. Query should contain \'label\' or \'denomination\'")
		}
	}))

module.exports = router
