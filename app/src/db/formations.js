/**
 * Database layer for the GDP resources.
 * This layer is using a CSV ile as the data source.
 */
const {datas} = require("../utils/data-provider");

/**
 * Find like label
 * @param {string} label
 * @returns {Array<Formation>} Formation resources
 */
const findLikeLabel = (label) => {
	return datas.filter((entry) => entry.label.toLowerCase().includes(label.toLowerCase()))
}

/**
 * Find like denomination
 * @param {string} denomination
 * @returns {Array<Formation>} Formation resources
 */
const findLikeDenomination = (denomination) => {
	return datas.filter((entry) => entry.denomination.toLowerCase().includes(denomination.toLowerCase()))
}

module.exports = {
	findLikeLabel,
	findLikeDenomination
}
