const NestedError = require('nested-error-stacks')

class RequestNotValidError extends NestedError {
	constructor(msg, err = null) {
		super(msg, err)
	}
}

RequestNotValidError.prototype.name = RequestNotValidError
module.exports = RequestNotValidError
