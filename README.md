# Typescript Exercises
**using NodeJS 16.0**

This repository aim to give you a step-by-step introduction to Typescript. You will build convert a javascript project to typescript, fix the errors, and add new features in Typescript.

To start the project you need to clone `master` branch then go through the exercises listed below.
You can always `git checkout -b exercise-###` to check or compare your solutions with the solutions we made. Not only one solution is possible as some exercises can be solved in multiple ways.


## Exercises
### 1. Convert the project to typescript
Convert your project to Typescript and fix all errors of compilation.

To be able to make this project using typescript, we will need to convert the project by changing all files extension from `*.js` to `*.ts` and add some configurations and libraries to help compiling the project.
To help you during this phase, you can start by reading those blogs that will help you understand the most common configuration

* [Migrate to Typescript on Node.js](https://dev.to/natelindev/migrate-to-typescript-on-node-js-2jhg)
* [How to Convert Node.js Code from JavaScript to TypeScript](https://javascript.plainenglish.io/how-to-convert-node-js-code-from-javascript-to-typescript-8e7d031a8f49)

Some other very nice resources to help you design you Typescript project:
* [5 TypeScript Design Patterns You Should Know](https://blog.bitsrc.io/design-patterns-in-typescript-e9f84de40449)
* [The Singleton Pattern In TypeScript](https://blog.bitsrc.io/the-singleton-pattern-in-typescript-b906303fda93)

At this point, you should have installed at least:
- `typescript` (globally)
- `ts-node-dev` (locally)
- `ts-node` (locally)
- `tslint` (locally)
- `@types/node@16 @types/express@4` (locally)

#### Warnings and check points
1. Pay attention to the style of your module exports and requires. With TS, it is encouraged to use `import` and `export default` on classes and/or modules.
2. Don't forget to add the dependencies of the types for the libs you are using such as `express` or any other library that you might be using. They are usually named `@types/express`, `@types/toto`, ...
3. In your package.json, when you are using `ts-node-dev` make sure you don't set the option `--transpile-only` otherwise you won't get any compiling error. You could use something like this
   ```"dev": "ts-node-dev --respawn ./index.ts",```.

### 2 Add a route to add a formation
Add a route to add a `formation` object with the payload you can find in the `Formation` type.
More precisely:
1. Add a route to the formation controller to express that will take the right payload
2. Add the services methods, and the db methods to be able to add to the data repository.
4. The id of the formation will have to be generated (uuid) in the DB repository.

Just a reminder, the payload should take a json payload that will correspond to this type:
```typescript
export type Formation = {
  id: string
  denomination: string
  siren: string
  siret: string
  voie: string
  code_postal: string
  ville: string
  label: string
}
```

### 3 Add a route to get a formation by id
Add a route to get a `formation` by id. The id a string uuid.
More precisely:
1. Add a route to the formation controller to express that will get the right request param: here the `id`
2. Add the services methods, and the db methods according to our needs
3. Add an error `FormationNotFoundError` is the object is not found and don't forget to return 404 when the error is found by the controller-advice

### 4 Implement an interface with generics
You will add an interface that our formation db repository file will implement
More precisely:
1. Add a file in `./db` folder named `data-repository.ts`.
2. Export an interface named `IDataRepository` that implements two methods
  1. `findById` that accept a `string` as input param and return an object
  2. `create` that accept an object as input param and return the same object.
3. Make your formation db repository implements that new interface.

**NOTE**: Use generics, not `any`, and not the `Object` type. (see [Typescript Generics](https://www.typescriptlang.org/docs/handbook/2/generics.html))

